# sample-node

[![GitHub release](https://img.shields.io/github/release/nalbam/sample-node.svg)](https://github.com/nalbam/sample-node/releases)
[![Build-Push](https://github.com/nalbam/sample-node/actions/workflows/push.yaml/badge.svg)](https://github.com/nalbam/sample-node/actions/workflows/push.yaml)
[![CircleCI](https://circleci.com/gh/nalbam/sample-node.svg?style=svg)](https://circleci.com/gh/nalbam/sample-node)

[![DockerHub Badge](http://dockeri.co/image/nalbam/sample-node)](https://hub.docker.com/r/nalbam/sample-node/)

## Docker

```bash
docker pull nalbam/sample-node
```

## npm install

```bash
npm install --save cors ejs express moment-timezone redis
npm install --save-dev jest
```
